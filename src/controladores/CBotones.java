package controladores;

import modelos.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.*;
import javax.imageio.ImageIO;
import javax.swing.*;
import modelos.figuras.*;
import vistas.*;

public class CBotones extends KeyAdapter implements ActionListener {

    private final PanelPrincipal panelPrincipal;
    private final PanelControles panelControles;
    private final PanelFiguras panelFiguras;
    private final PanelRaster panelRaster;
    private final Raster raster;
    private Color color;
    private String tipoTransformacion;
    private Matrix matrix;

    public CBotones(Raster raster, PanelPrincipal panelPrincipal) {
        this.panelPrincipal = panelPrincipal;
        panelControles = panelPrincipal.getPanelControles();
        panelFiguras = panelPrincipal.getPanelFiguras();
        panelRaster = panelPrincipal.getPanelRaster();
        color = panelControles.getColor();
        this.raster = raster;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        JComponent botones = (JComponent) e.getSource();
        switch (botones.getName()) {
            case "btnGuardarRast":
                guardarImagen();
                break;
            case "btnGuardarVect":
                guardarVectores();
                break;
            case "btnColor":
                selectorColores();
                break;
            case "btnBorrar":
                borrarFiguras();
                break;
            case "btnTrasladar":
                aplicarTraslacion();
                break;
            case "btnEscalar":
                aplicarEscalamiento();
                break;
            case "btnRotar":
                aplicarRotacion();
        }
        panelControles.requestFocus();
    }

    @Override
    public void keyReleased(KeyEvent e) {
        seleccionarFigura(e);
    }

    private void aplicarTraslacion() {
        int vCampoTX = Integer.parseInt(panelControles.getCampoTX().getText());
        int vCampoTY = Integer.parseInt(panelControles.getCampoTY().getText());
        matrix = aplicarMatrixTransformacion("trasladar", vCampoTX, vCampoTY, 0);
        establecerFiguraTransformacion();
    }

    private void aplicarEscalamiento() {
        int vCampoEX = Integer.parseInt(panelControles.getCampoSX().getText());
        int vCampoEY = Integer.parseInt(panelControles.getCampoSY().getText());
        matrix = aplicarMatrixTransformacion("escalar", vCampoEX, vCampoEY, 0);
        establecerFiguraTransformacion();
    }

    private void aplicarRotacion() {
        int vCampoRA = Integer.parseInt(panelControles.getCampoRA().getText());
        matrix = aplicarMatrixTransformacion("rotar", 0, 0, vCampoRA);
        establecerFiguraTransformacion();
    }

    private Matrix aplicarMatrixTransformacion(String tipoTransformacion, int x, int y, int ang) {
        Matrix matrix = new Matrix();
        this.tipoTransformacion = tipoTransformacion;
        switch (tipoTransformacion) {
            case "trasladar":
            case "escalar":
                if (tipoTransformacion.equals("trasladar")) {
                    matrix.traslacion(x, y);
                    System.out.println("\nENTRO A TRASLADAR");
                } else {
                    matrix.escalarXY(x, y);
                    System.out.println("\nENTRO A ESCALAR");
                }
                break;
            case "rotar":
                System.out.println("\nENTRO A ROTAR");
                matrix.rotacion(ang);
        }
        return matrix;
    }

    private ArrayList<Point> obtenerPPrima(Point p1, Point p2, Point p3, Point p4) {
        System.out.println("\nPUNTOS ORIGINALES");
        System.out.println("p1.x: " + p1.x + " p1.y: " + p1.y);
        System.out.println("p2.x: " + p2.x + " p2.y: " + p2.y);
        System.out.println("p3.x: " + p3.x + " p3.y: " + p3.y);
        System.out.println("p4.x: " + p4.x + " p4.y: " + p4.y);

        double[] pO1 = {p1.x, p1.y, 1};
        double[] pO2 = {p2.x, p2.y, 1};
        double[] pO3 = {p3.x, p3.y, 1};
        double[] pO4 = {p4.x, p4.y, 1};

        double[] pp1 = matrix.pprima(pO1);
        double[] pp2 = matrix.pprima(pO2);
        double[] pp3 = matrix.pprima(pO3);
        double[] pp4 = matrix.pprima(pO4);

        Point ppt1 = new Point((int) pp1[0], (int) pp1[1]);
        Point ppt2 = new Point((int) pp2[0], (int) pp2[1]);
        Point ppt3 = new Point((int) pp3[0], (int) pp3[1]);
        Point ppt4 = new Point((int) pp4[0], (int) pp4[1]);

//        Point ppt1 = new Point(Math.abs((int) pp1[0]), Math.abs((int) pp1[1]));
//        Point ppt2 = new Point(Math.abs((int) pp2[0]), Math.abs((int) pp2[1]));
//        Point ppt3 = new Point(Math.abs((int) pp3[0]), Math.abs((int) pp3[1]));
        System.out.println("\nPUNTOS OBTENIDOS AL APLICAR MATRIX");
        System.out.println("ph1.x: " + ppt1.x + " ph1.y: " + ppt1.y);
        System.out.println("ph2.x: " + ppt2.x + " ph2.y: " + ppt2.y);
        System.out.println("ph3.x: " + ppt3.x + " ph3.y: " + ppt3.y);
        System.out.println("ph4.x: " + ppt4.x + " ph4.y: " + ppt4.y);

        if (tipoTransformacion.equals("rotar")) {
            /*Primero se aplica la matriz de rotacion, posteriormente traslado la figura a su punto origen
            aplicando ahora una matriz de traslacion*/
            int xT = (p1.x - ppt1.x);
            int yT = (p1.y - ppt1.y);
            Matrix matrix = new Matrix();
            matrix.traslacion(xT, yT);
            double[] pp1h = {ppt1.x, ppt1.y, 1};
            double[] pp2h = {ppt2.x, ppt2.y, 1};
            double[] pp3h = {ppt3.x, ppt3.y, 1};
            double[] pp4h = {ppt4.x, ppt4.y, 1};

            pp1 = matrix.pprima(pp1h);
            pp2 = matrix.pprima(pp2h);
            pp3 = matrix.pprima(pp3h);
            pp4 = matrix.pprima(pp4h);

            ppt1.x = (int) pp1[0];
            ppt1.y = (int) pp1[1];

            ppt2.x = (int) pp2[0];
            ppt2.y = (int) pp2[1];

            ppt3.x = (int) pp3[0];
            ppt3.y = (int) pp3[1];
            
            ppt4.x = (int) pp4[0];
            ppt4.y = (int) pp4[1];
        }

        ArrayList<Point> pp = new ArrayList<>();

        pp.add(ppt1);
        pp.add(ppt2);
        pp.add(ppt3);
        pp.add(ppt4);

        return pp;
    }

    private void establecerFiguraTransformacion() {
        JList figuraSeleccionada = panelFiguras.getListVectoresFiguras();
        int indice = figuraSeleccionada.getSelectedIndex();
        ArrayList<Figura> figuras = panelPrincipal.getAFiguras();
        Figura f = figuras.get(indice);
        Point p0 = new Point(0, 0);
        for (int i = 0; i < figuras.size(); i++) {
            if (f instanceof Linea) {
                Linea l = (Linea) f;

                ArrayList<Point> pp = obtenerPPrima(l.getPunto1(), l.getPunto2(), p0, p0);

                Point pp1 = pp.get(0);
                Point pp2 = pp.get(1);

                Linea lh = new Linea(pp1, pp2, color);
                lh.setRaster(raster);
                lh.dibujarLineFast(pp1.x, pp1.y, pp2.x, pp2.y, color);

                panelPrincipal.getAFiguras().add(lh);
                panelFiguras.getListVectoresModel().addElement("Linea (" + tipoTransformacion + ")");
                panelRaster.repaint();
                break;
            }
            if (f instanceof TrianguloR) {
                TrianguloR t = (TrianguloR) f;

                ArrayList<Point> pp = obtenerPPrima(t.getPunto1(), t.getPunto2(), t.getPunto3(), p0);

                Point pp1 = pp.get(0);
                Point pp2 = pp.get(1);
                Point pp3 = pp.get(2);

                Vertex2D v1 = new Vertex2D(pp1.x, pp1.y, color.getRGB());
                Vertex2D v2 = new Vertex2D(pp2.x, pp2.y, color.getRGB());
                Vertex2D v3 = new Vertex2D(pp3.x, pp3.y, color.getRGB());

                TrianguloR th = new TrianguloR(v1, v2, v3, color);
                th.dibujar(raster);

                panelPrincipal.getAFiguras().add(th);
                panelFiguras.getListVectoresModel().addElement("Triangulo (" + tipoTransformacion + ")");
                panelRaster.repaint();
                break;
            }
            if (f instanceof Rectangulo) {
                Rectangulo r = (Rectangulo) f;

                ArrayList<Point> pp = obtenerPPrima(r.getPunto1(), r.getPunto2(),
                        r.getPunto3(), r.getPunto4());

                Point pp1 = pp.get(0);
                Point pp2 = pp.get(1);
                Point pp3 = pp.get(2);
                Point pp4 = pp.get(3);

                Rectangulo rh = new Rectangulo(pp1, pp2, pp3, pp4, color);
                rh.dibujar(raster);

                panelPrincipal.getAFiguras().add(rh);
                panelFiguras.getListVectoresModel().addElement("Rectangulo (" + tipoTransformacion + ")");
                panelRaster.repaint();
                break;
            }
            if (f instanceof Circulo) {
                Circulo c = (Circulo) f;

                ArrayList<Point> pp = obtenerPPrima(c.getPunto1(), c.getPunto2(), p0, p0);

                Point pp1 = pp.get(0);
                Point pp2 = pp.get(1);

                Circulo ch = new Circulo(pp1, pp2, color);
                ch.drawCircleBresenhamInt(raster);

                panelPrincipal.getAFiguras().add(ch);
                panelFiguras.getListVectoresModel().addElement("Circulo (" + tipoTransformacion + ")");
                panelRaster.repaint();
                break;
            }
            
            if (f instanceof Elipse) {
                Elipse e = (Elipse) f;

                ArrayList<Point> pp = obtenerPPrima(e.getPunto1(), e.getPunto2(), p0, p0);

                Point pp1 = pp.get(0);
                Point pp2 = pp.get(1);

                Elipse eh = new Elipse(pp1, pp2, color);
                eh.drawEllipseBresenham(raster);

                panelPrincipal.getAFiguras().add(eh);
                panelFiguras.getListVectoresModel().addElement("Elipse (" + tipoTransformacion + ")");
                panelRaster.repaint();
                break;
            }
        }//FIN CICLO FOR
    }

    private void selectorColores() {
        color = JColorChooser.showDialog(null, "Seleccione un color", color);
        panelControles.getBtnColor().setBackground(color);
        panelControles.setColor(color);
    }

    public static BufferedImage toBufferedImage(Image img) {
        if (img instanceof BufferedImage) {
            return (BufferedImage) img;
        }

        // Create a buffered image with transparency
        BufferedImage bimage = new BufferedImage(img.getWidth(null), img.getHeight(null), BufferedImage.TYPE_INT_ARGB);

        // Draw the image on to the buffered image
        Graphics2D bGr = bimage.createGraphics();
        bGr.drawImage(img, 0, 0, null);
        bGr.dispose();

        // Return the buffered image
        return bimage;
    }

    public static Color hex2Rgb(String colorStr) {
        return new Color(
                Integer.valueOf(colorStr.substring(1, 3), 16),
                Integer.valueOf(colorStr.substring(3, 5), 16),
                Integer.valueOf(colorStr.substring(5, 7), 16));
    }

    public void guardarImagen() {

        BufferedImage img = toBufferedImage(raster.toImage(panelRaster));
        try {
            File outputfile = new File("saved.png");
            ImageIO.write(img, "png", outputfile);
        } catch (IOException e) {

        }
    }

    public void guardarVectores() {
        FileWriter fw = null;
        String figura = "";

        try {
            fw = new FileWriter("vectores.txt");
            ArrayList<Figura> aFiguras = panelPrincipal.getAFiguras();
            for (int i = 0; i < aFiguras.size(); i++) {
                Figura f = aFiguras.get(i);

                if (f instanceof Linea) {
                    Linea l = (Linea) f;
                    figura = String.format("L,%.0f,%.0f,%.0f,%.0f,%x\n", l.getPunto1().getX(), l.getPunto1().getY(),
                            l.getPunto2().getX(), l.getPunto2().getY(),
                            l.getColor().getRGB());
                }

                if (f instanceof TrianguloR) {
                    TrianguloR t = (TrianguloR) f;
                    Vertex2D[] v = t.getVertex2D();
                    figura = String.format("T,%d,%d,%d,%d,%d,%d,%x\n", v[0].getX(), v[0].getY(), v[1].getX(), v[1].getY(), v[2].getX(), v[2].getY(),
                            t.getColorInt());
                }

                if (f instanceof Circulo) {
                    Circulo c = (Circulo) f;
                    figura = String.format("C,%.0f,%.0f,%.0f,%.0f,%x\n", c.getPunto1().getX(), c.getPunto1().getY(),
                            c.getPunto2().getX(), c.getPunto2().getY(),
                            c.getColor().getRGB());
                }
                
                if (f instanceof Elipse) {
                    Elipse e = (Elipse) f;
                    figura = String.format("E,%.0f,%.0f,%.0f,%.0f,%x\n", e.getPunto1().getX(), e.getPunto1().getY(),
                            e.getPunto2().getX(), e.getPunto2().getY(),
                            e.getColor().getRGB());
                }

                if (f instanceof Rectangulo) {
                    Rectangulo r = (Rectangulo) f;
                    figura = String.format("R,%.0f,%.0f,%.0f,%.0f,%.0f,%.0f,%.0f,%.0f,%x\n", r.getPunto1().getX(), r.getPunto1().getY(),
                            r.getPunto2().getX(), r.getPunto2().getY(), r.getPunto3().getX(), r.getPunto3().getY(),
                            r.getPunto4().getX(), r.getPunto4().getY(), r.getColor().getRGB());
                }

                fw.write(figura);
            }
        } catch (IOException ex) {
//            Logger.getLogger(Pizarra2.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        } finally {
            try {
                fw.close();
            } catch (IOException ex) {
//                Logger.getLogger(Pizarra2.class.getName()).log(Level.SEVERE, null, ex);
                ex.printStackTrace();
            }
        }
    }

    public void seleccionarFigura(KeyEvent ke) {
        String figura = "";
        switch (ke.getKeyCode()) {
            case KeyEvent.VK_T:
                figura = "Triangulo";
                break;
            case KeyEvent.VK_L:
                figura = "Linea";
                break;
            case KeyEvent.VK_R:
                figura = "Rectangulo";
                break;
            case KeyEvent.VK_C:
                figura = "Circulo";
                break;
            case KeyEvent.VK_E:
                figura = "Elipse";
        }
        seleccionarBoton(figura);
    }

    public final void seleccionarBoton(String figura) {
        ButtonGroup grupoBotones = panelControles.getGrupoBotones();
        Enumeration<AbstractButton> botones = grupoBotones.getElements();
        while (botones.hasMoreElements()) {
            AbstractButton boton = botones.nextElement();
            if (boton.getActionCommand().equals(figura)) {
                boton.setSelected(true);
            }
        }
    }

    public void clear() {
        int s = raster.size();
        for (int i = 0; i < s; i++) {
            raster.getPixel()[i] ^= 0x00ffffff;
        }
        panelRaster.repaint();
        return;
    }

    public void borrarFiguras() {
        panelPrincipal.getAFiguras().clear();
        panelFiguras.getListVectoresModel().clear();
        //Borrando pixeles de figuras dibujadas
        raster.setPixel(new int[raster.getPixel().length]);
        panelRaster.repaint();
    }

}
