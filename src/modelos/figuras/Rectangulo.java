package modelos.figuras;

import java.awt.*;
import modelos.Raster;

public class Rectangulo extends Figura {
    
    private Point p1;
    private Point p2;
    private Point p3;
    private Point p4;
    private Linea linea;
    
    public Rectangulo(Point p1, Point p2, Color c) {
        this.p1 = p1;
        this.p2 = p2;
        this.p3 = new Point(p2.x, p1.y);
        this.p4 = new Point(p1.x, p2.y);
        color = c;
        linea = new Linea(p1, p2, c);
    }
    
    public Rectangulo(Point p1, Point p2, Point p3, Point p4, Color c) {
        this.p1 = p1;
        this.p2 = p2;
        this.p3 = p3;
        this.p4 = p4;
        color = c;
        linea = new Linea(p1, p2, c);
    }
    
    public void dibujar(Raster raster) {
        linea.setRaster(raster);
        linea.dibujarLineFast(p1.x, p1.y, p3.x, p3.y, color);
        linea.dibujarLineFast(p3.x, p3.y, p2.x, p2.y, color);
        linea.dibujarLineFast(p2.x, p2.y, p4.x, p4.y, color);
        linea.dibujarLineFast(p4.x, p4.y, p1.x, p1.y, color);
    }
    
    public Point getPunto1() {
        return p1;
    }
    
    public Point getPunto2() {
        return p2;
    }
    
    public Point getPunto3() {
        return p3;
    }
    
    public Point getPunto4() {
        return p4;
    }

}
