package modelos.figuras;

import java.awt.Color;
import java.awt.Point;
import modelos.Raster;

public class Elipse extends Figura {

    private Point p1;
    private Point p2;

    public Elipse(Point p1, Point p2, Color c) {
        this.p1 = p1;
        this.p2 = p2;
        color = c;
    }

    /**
     * Helper function to draw 4 symmteric points of an ellipse.
     *
     * @param g Graphics object used for actual pixel plotting
     * @param mx x-coordinate of center of ellipse
     * @param my y-coordinate of center of ellipse
     * @param x x-value for point on ellipse from center
     * @param y y-value for point on ellipse from center
     * @param multiplier zoom multiplier, use 1 for normal drawing
     */
    private void drawSymmetricPoints(Raster raster, int mx, int my, int x, int y) {
        int c = color.getRGB();
        raster.setPixel(c, (mx + x), (my + y));
        raster.setPixel(c, (mx - x), (my + y));
        raster.setPixel(c, (mx - x), (my - y));
        raster.setPixel(c, (mx + x), (my - y));
    }

    /**
     * Bresenham ellipse rasterising algorithm, using integer numbers.
     *
     * @param g Graphics object where ellipse is plotted
     * @param mx x-coordinate of center of ellipse
     * @param my y-coordinate of center of ellipse
     * @param radiusX radius in x-direction of ellipse
     * @param radiusY radius in y-direction of ellipse
     * @param multiplier zoom multiplier, use 1 for normal drawing
     */
    public void drawEllipseBresenham(Raster g) {

        int multiplier = 1;

        double dX = Math.pow((Math.abs(p2.x - p1.x)), 2);
        double dY = Math.pow((Math.abs(p2.y - p1.y)), 2);
        int d = (int) (Math.sqrt(dX + dY));
        int radiusX = Math.abs(p2.x - p1.x);
        int radiusY = Math.abs(p2.y - p1.y);
//        raiudsY = Math.abs(this.punto2.x-this.punto1.x) - Math.abs(this.punto2.y-this.punto1.y);

        int x, y, changeX, changeY, e, twoASquare, twoBSquare, stoppingX, stoppingY, mx, my;

        mx = (p1.x + p2.x) / 2;
        my = (p1.y + p2.y) / 2;

        mx /= multiplier;
        my /= multiplier;
        radiusX /= multiplier;
        radiusY /= multiplier;

        twoASquare = 2 * radiusX * radiusX;
        twoBSquare = 2 * radiusY * radiusY;
        x = radiusX;
        y = 0;
        changeX = radiusY * radiusY * (1 - 2 * radiusX);
        changeY = radiusX * radiusX;
        e = 0;
        stoppingX = twoBSquare * radiusX;
        stoppingY = 0;

        while (stoppingX >= stoppingY) {
            drawSymmetricPoints(g, mx, my, x, y);
            y++;
            stoppingY += twoASquare;
            e += changeY;
            changeY += twoASquare;
            if ((2 * e + changeX) > 0) {
                x--;
                stoppingX -= twoBSquare;
                e += changeX;
                changeX += twoBSquare;
            }
        }

        x = 0;
        y = radiusY;
        changeX = radiusY * radiusY;
        changeY = radiusX * radiusX * (1 - 2 * radiusY);
        e = 0;
        stoppingX = 0;
        stoppingY = twoASquare * radiusY;
        while (stoppingX <= stoppingY) {
            drawSymmetricPoints(g, mx, my, x, y);
            x++;
            stoppingX += twoBSquare;
            e += changeX;
            changeX += twoBSquare;
            if ((2 * e + changeY) > 0) {
                y--;
                stoppingY -= twoASquare;
                e += changeY;
                changeY += twoASquare;
            }
        }
    }

    public Point getPunto1() {
        return p1;
    }

    public Point getPunto2() {
        return p2;
    }

}
