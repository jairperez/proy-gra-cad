package modelos;

public class Vertex2D {

    private int x;
    private int y;              // coordenada oe vertex
    private int argb;              // color de vertex

    public Vertex2D(int xval, int yval, int cval) {
        x = xval;
        y = yval;
        argb = cval;
    }

    /**
     * @return the x
     */
    public int getX() {
        return x;
    }

    /**
     * @param x the x to set
     */
    public void setX(int x) {
        this.x = x;
    }

    /**
     * @return the y
     */
    public int getY() {
        return y;
    }

    /**
     * @param y the y to set
     */
    public void setY(int y) {
        this.y = y;
    }

    /**
     * @return the argb
     */
    public int getArgb() {
        return argb;
    }

    /**
     * @param argb the argb to set
     */
    public void setArgb(int argb) {
        this.argb = argb;
    }
}
