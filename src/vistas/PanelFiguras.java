package vistas;

import controladores.CBotones;
import java.awt.Dimension;
import javax.swing.*;

public class PanelFiguras extends JPanel {

    private JList listFiguras;
    private DefaultListModel listModel;
    private JButton btnGuardarVect;
    private JScrollPane scrollFiguras;

    public PanelFiguras() {
        addComponentes();
    }

    public final void addComponentes() {
        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));

        btnGuardarVect = new JButton("Guardar Vector");

        btnGuardarVect.setName("btnGuardarVect");

        scrollFiguras = new JScrollPane();

        listFiguras = new JList();
        listFiguras.setModel(new DefaultListModel());
        listModel = (DefaultListModel) listFiguras.getModel();

        scrollFiguras.setViewportView(listFiguras);

        scrollFiguras.setPreferredSize(new Dimension(50, 100));
        add(scrollFiguras);
        add(new JSeparator());
        add(btnGuardarVect);
    }
    
    public void addEventos(CBotones cBotones) {
        btnGuardarVect.addActionListener(cBotones);
    }

    /**
     * @return the listFiguras
     */
    public JList getListVectoresFiguras() {
        return listFiguras;
    }

    /**
     * @return the listModel
     */
    public DefaultListModel getListVectoresModel() {
        return listModel;
    }

}
