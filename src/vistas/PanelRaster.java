package vistas;

import controladores.CPanelRaster;
import java.awt.*;
import javax.swing.*;
import modelos.Raster;

public class PanelRaster extends JPanel {

    private Raster raster;
    private Dimension prefSize;
    private Graphics g;

    public PanelRaster() {
        setBorder(BorderFactory.createLineBorder(Color.black));
    }

    public PanelRaster(Raster raster) {
        prefSize = new Dimension(raster.getWidth(), raster.getHeight());
        this.setSize(prefSize);
        this.raster = raster;
        setBorder(BorderFactory.createLineBorder(Color.black));
    }
    
    public void addEventos(CPanelRaster cPanelRaster) {
        addMouseListener(cPanelRaster);
    }

    @Override
    public Dimension getPreferredSize() {
        return prefSize;
    }

    public void setRaster(Raster _raster) {
        this.raster = _raster;
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        Image output = raster.toImage(this);
        g.drawImage(output, 0, 0, this);
    }
    
}
